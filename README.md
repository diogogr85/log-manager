Log Manger
==========

## Getting started

### Dependency

In Project's build.gradle add this line:

```groovy
allprojects {
    repositories {
        ...
        maven { url "https://dl.bintray.com/diogogr85/log-manager" }
    }
}
```

In Module's build.gradle include the dependency:

```groovy
dependencies {
    compile 'LogManager:log-manager-1.0.3:1.0.3'
}
```

## How to use

Just call LogManager methods, passing a file url or a local log file (.txt)

###Summary

```java
LogManager.generateLogSummary(fileUrl, new LogReportsCallback() {
        @Override
        public void onReportsSuccess(String reportsJson) {
            ...
        }

        @Override
        public void onReportsFailed(Integer statusCode, String errorMessage) {
            ...
        }
});

LogManager.generateLogSummary(file, new LogReportsCallback() {
        @Override
        public void onReportsSuccess(String reportsJson) {
            ...
        }

        @Override
        public void onReportsFailed(Integer statusCode, String errorMessage) {
            ...
        }
});
```

###Ranking

```java
LogManager.generateLogRanking(fileUrl, new LogReportsCallback() {
        @Override
        public void onReportsSuccess(String reportsJson) {
            ...
        }

        @Override
        public void onReportsFailed(Integer statusCode, String errorMessage) {
            ...
        }
});

LogManager.generateLogRanking(file, new LogReportsCallback() {
        @Override
        public void onReportsSuccess(String reportsJson) {
            ...
        }

        @Override
        public void onReportsFailed(Integer statusCode, String errorMessage) {
            ...
        }
});
```
Some response examples:

Ranking: https://bytebucket.org/diogogr85/log-manager/raw/d4171d2a2033b7f72e3dc8e18ff84da9f38e27b6/docs/ranking.png?token=1d573a6a562ea68034a8b1273b2601eb6df85218

Summary: https://bytebucket.org/diogogr85/log-manager/raw/d4171d2a2033b7f72e3dc8e18ff84da9f38e27b6/docs/summary.png?token=9495f0cd14d56fd799907cfd7236eb3983305581