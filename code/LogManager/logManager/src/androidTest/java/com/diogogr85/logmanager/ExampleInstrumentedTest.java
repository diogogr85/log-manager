package com.diogogr85.logmanager;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.util.Log;

import com.diogogr85.logmanager.data.LogManager;
import com.diogogr85.logmanager.data.LogReportsCallback;
import com.diogogr85.logmanager.data.analytics.AnalyticsCallback;
import com.diogogr85.logmanager.data.models.FilteredLog;
import com.diogogr85.logmanager.data.models.LogReport;
import com.diogogr85.logmanager.data.models.RankingReport;
import com.diogogr85.logmanager.data.dataManagement.FileManagerTask;
import com.diogogr85.logmanager.data.dataManagement.DataManagementCallback;
import com.diogogr85.logmanager.data.network.NetworkApiFacade;
import com.diogogr85.logmanager.data.network.base.NetworkCall;
import com.diogogr85.logmanager.utils.JsonUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.diogogr85.logmanager", appContext.getPackageName());
    }

    @Test
    public void genarateSummaryByLocalFile() {
        File file = new File("file:///android_asset/log.txt");

        LogManager.generateLogSummary(file,
                new LogReportsCallback() {
                    @Override
                    public void onReportsSuccess(String reportsJson) {
                        Log.d("REPORT-SUMMARY", reportsJson);
                    }

                    @Override
                    public void onReportsFailed(Integer statusCode, String errorMessage) {
                        Log.d("REPORT-SUMMARY-ERROR", "status code: " + statusCode + "; error: " + errorMessage);
                    }
                });
    }

    @Test
    public void generateSummaryByDownloading() {
        final String url = "https://gist.githubusercontent.com/diogogr85/856fb1baa6f8ff5c0fa2d129bf1626d7/raw/2d1b38a3f6136928412bb3a8c61dbf73f9075005/log.txt";

        LogManager.generateLogRanking(url, new LogReportsCallback() {
            @Override
            public void onReportsSuccess(String reportsJson) {
                Log.d("REPORT-SUMMARY", reportsJson);
            }

            @Override
            public void onReportsFailed(Integer statusCode, String errorMessage) {
                Log.d("REPORT-SUMMARY-ERROR", "status code: " + statusCode + "; error: " + errorMessage);
            }
        });
    }

    @Test
    public void generateRankingByLocalFile() {
        File file = new File("file:///android_asset/log.txt");

        LogManager.generateLogRanking(file, new LogReportsCallback() {
            @Override
            public void onReportsSuccess(String reportsJson) {
                Log.d("REPORT-RANKING", reportsJson);
            }

            @Override
            public void onReportsFailed(Integer statusCode, String errorMessage) {
                Log.d("REPORT-RANKING-ERROR", "status code: " + statusCode + "; error: " + errorMessage);
            }
        });
    }

    @Test
    public void generateRankingByDownloading() {
        final String url = "https://gist.githubusercontent.com/diogogr85/856fb1baa6f8ff5c0fa2d129bf1626d7/raw/2d1b38a3f6136928412bb3a8c61dbf73f9075005/log.txt";

        LogManager.generateLogRanking(url,
                new LogReportsCallback() {
                    @Override
                    public void onReportsSuccess(String reportsJson) {
                        Log.d("REPORT-RANKING", reportsJson);
                    }

                    @Override
                    public void onReportsFailed(Integer statusCode, String errorMessage) {
                        Log.d("REPORT-RANKING-ERROR", "status code: " + statusCode + "; error: " + errorMessage);
                    }
                });

    }

}
