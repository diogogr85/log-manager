package com.diogogr85.logmanager;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by diogogr85 on 05/08/17.
 */

public class LogManagerApplication extends Application {

    private static LogManagerApplication sInstance;
    private static GsonBuilder sGsonBuilder;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        sGsonBuilder = new GsonBuilder();
    }

    public static LogManagerApplication getInstance() {
        return sInstance;
    }

    public static Gson getGsonInstance() {
        return sGsonBuilder.create();
    }

}
