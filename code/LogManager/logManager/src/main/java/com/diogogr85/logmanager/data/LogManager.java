package com.diogogr85.logmanager.data;

import com.diogogr85.logmanager.data.analytics.Analytics;
import com.diogogr85.logmanager.data.analytics.AnalyticsCallback;
import com.diogogr85.logmanager.data.dataManagement.FileManagerTask;
import com.diogogr85.logmanager.data.dataManagement.DataManagementCallback;
import com.diogogr85.logmanager.data.models.FilteredLog;
import com.diogogr85.logmanager.data.models.LogReport;
import com.diogogr85.logmanager.data.models.RankingReport;
import com.diogogr85.logmanager.data.network.NetworkApiFacade;
import com.diogogr85.logmanager.data.network.base.NetworkCall;
import com.diogogr85.logmanager.utils.JsonUtils;

import java.io.File;
import java.util.List;

/**
 * Created by diogogr85 on 06/08/17.
 */

public class LogManager {

    public static void generateLogSummary(final String fileUrl, final LogReportsCallback callback) {
        NetworkApiFacade.downloadFile(fileUrl,
                new NetworkCall.Callback<File>() {
                    @Override
                    public void onSuccess(File result) {
                        try {
                            final FileManagerTask fileManager = new FileManagerTask(new DataManagementCallback() {
                                @Override
                                public void onReadFileSuccess(FilteredLog log) {
                                    Analytics.build(log).summary(new AnalyticsCallback<LogReport>() {
                                        @Override
                                        public void onAnalyticsReportSuccess(LogReport report) {
                                            callback.onReportsSuccess(JsonUtils.toLogReportJson(report));
                                        }
                                    });
                                }
                            });
                            fileManager.execute(result.getAbsolutePath());

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                            callback.onReportsFailed(null, e.getMessage());
                        }
                    }

                    @Override
                    public void onError(Integer statusCode, String errors) {
                        callback.onReportsFailed(statusCode, errors);
                    }
                });
    }

    public static void generateLogSummary(final File file, final LogReportsCallback callback) {
        try {
            final FileManagerTask fileManager = new FileManagerTask(new DataManagementCallback() {
                @Override
                public void onReadFileSuccess(FilteredLog log) {
                    Analytics.build(log).summary(new AnalyticsCallback<LogReport>() {
                        @Override
                        public void onAnalyticsReportSuccess(LogReport report) {
                            callback.onReportsSuccess(JsonUtils.toLogReportJson(report));
                        }
                    });
                }
            });
            fileManager.execute(file.getAbsolutePath());
        } catch (NullPointerException e) {
            e.printStackTrace();
            callback.onReportsFailed(null, e.getMessage());
        }
    }

    public static void generateLogRanking(final String fileurl, final LogReportsCallback callback) {
        NetworkApiFacade.downloadFile(fileurl,
                new NetworkCall.Callback<File>() {
                    @Override
                    public void onSuccess(File result) {
                        final FileManagerTask fileManager = new FileManagerTask(new DataManagementCallback() {
                            @Override
                            public void onReadFileSuccess(FilteredLog log) {
                                Analytics.build(log).ranking(new AnalyticsCallback<List<RankingReport>>() {
                                    @Override
                                    public void onAnalyticsReportSuccess(List<RankingReport> report) {
                                        callback.onReportsSuccess(JsonUtils.toRankingReportJson(report));
                                    }
                                });
                            }
                        });
                        fileManager.execute(result.getAbsolutePath());
                    }

                    @Override
                    public void onError(Integer statusCode, String errors) {
                        callback.onReportsFailed(statusCode, errors);
                    }
                });


    }

    public static void generateLogRanking(final File file, final LogReportsCallback callback) {
        final FileManagerTask fileManager = new FileManagerTask(new DataManagementCallback() {
            @Override
            public void onReadFileSuccess(FilteredLog log) {
                Analytics.build(log).ranking(new AnalyticsCallback<List<RankingReport>>() {
                    @Override
                    public void onAnalyticsReportSuccess(List<RankingReport> report) {
                        callback.onReportsSuccess(JsonUtils.toRankingReportJson(report));
                    }
                });
            }
        });
        fileManager.execute(file.getAbsolutePath());
    }

}
