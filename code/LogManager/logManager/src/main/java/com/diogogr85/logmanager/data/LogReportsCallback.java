package com.diogogr85.logmanager.data;

/**
 * Created by diogogr85 on 08/08/17.
 */

public interface LogReportsCallback {

    void onReportsSuccess(final String reportsJson);
    void onReportsFailed(final Integer statusCode, final String errorMessage);

}
