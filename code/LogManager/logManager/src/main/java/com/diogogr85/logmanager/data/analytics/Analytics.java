package com.diogogr85.logmanager.data.analytics;

import com.diogogr85.logmanager.data.models.FilteredGame;
import com.diogogr85.logmanager.data.models.FilteredLog;
import com.diogogr85.logmanager.data.models.FilteredPlayer;
import com.diogogr85.logmanager.data.models.KillReport;
import com.diogogr85.logmanager.data.models.LogReport;
import com.diogogr85.logmanager.data.models.GameSummary;
import com.diogogr85.logmanager.data.models.RankingReport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by diogogr85 on 06/08/17.
 */

public class Analytics {

    private final LogReport mLogReport;

    public Analytics(final FilteredLog log) {
        mLogReport = new LogReport();
        prepareInfo(log);
    }

    public static Analytics build(final FilteredLog log) {
        return new Analytics(log);
    }

    public void summary(final AnalyticsCallback<LogReport> callback) {

        callback.onAnalyticsReportSuccess(mLogReport);

    }

    public void ranking(final AnalyticsCallback<List<RankingReport>> callback) {

        final List<RankingReport> results = new ArrayList<>();

        for (GameSummary summary : mLogReport.getLogs()) {
            Collections.sort(summary.getGameLog().getKills(), new Comparator<KillReport>() {
                @Override
                public int compare(KillReport o1, KillReport o2) {
                    if (o1.getKills() > o2.getKills()) {
                        return 1;
                    } else if (o1.getKills() < o2.getKills()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            });

            final String title = "game_" + summary.getGameLog().getId();
            final RankingReport rankingReport = new RankingReport(title, summary.getGameLog().getKills());
            results.add(rankingReport);
        }

        callback.onAnalyticsReportSuccess(results);
    }

    private void prepareInfo(final FilteredLog log) {
        for (FilteredGame filteredGame : log.getGamesLogs()) {
            final GameSummary.Game game = new GameSummary.Game(filteredGame.getId());

            int totalKills = 0;
            for (FilteredPlayer filteredPlayer : filteredGame.getPlayers()) {
                totalKills += filteredPlayer.getKill();

                if (!filteredPlayer.isWorld()) {
                    final KillReport killReport = new KillReport(filteredPlayer.getName(), filteredPlayer.getKill());
                    game.addKills(killReport);

                    game.addPlayer(filteredPlayer.getName());
                }
            }
            game.setTotalKills(totalKills);


            final GameSummary gameSummary = new GameSummary(game);
            mLogReport.addSummary(gameSummary);
        }
    }

}
