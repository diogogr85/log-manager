package com.diogogr85.logmanager.data.analytics;

import com.diogogr85.logmanager.data.models.LogReport;

/**
 * Created by diogogr85 on 07/08/17.
 */

public interface AnalyticsCallback<T> {

    void onAnalyticsReportSuccess(final T report);

}
