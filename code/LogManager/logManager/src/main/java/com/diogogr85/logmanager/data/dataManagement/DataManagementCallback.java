package com.diogogr85.logmanager.data.dataManagement;

import com.diogogr85.logmanager.data.models.FilteredLog;
import com.diogogr85.logmanager.data.models.RawLog;

/**
 * Created by diogogr85 on 04/08/17.
 */

public interface DataManagementCallback {

    void onReadFileSuccess(final FilteredLog log);

}
