package com.diogogr85.logmanager.data.dataManagement;

import android.os.AsyncTask;

import com.diogogr85.logmanager.LogManagerApplication;
import com.diogogr85.logmanager.data.dataManagement.helpers.FilterRawInfoHelper;
import com.diogogr85.logmanager.data.models.RawGameLog;
import com.diogogr85.logmanager.data.models.RawLog;
import com.diogogr85.logmanager.data.models.RawPlayerLog;
import com.diogogr85.logmanager.utils.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by diogogr85 on 04/08/17.
 */

public class FileManagerTask extends AsyncTask<String, Void, RawLog> {

    private static int COUNTER = -1;

    private final DataManagementCallback mCallback;

    public FileManagerTask(final DataManagementCallback callback) {
        mCallback = callback;
    }

    @Override
    protected RawLog doInBackground(String... params) {
        File file = new File(params[0]);

        try {
            final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;

            final RawLog rawLog = new RawLog();
            while ((line = bufferedReader.readLine()) != null) {
                evaluateLine(rawLog, line);
            }
            bufferedReader.close();

            return rawLog;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(RawLog rawLog) throws NullPointerException {
        super.onPostExecute(rawLog);
        COUNTER = -1;

        try {
            final FilterRawInfoHelper helper = FilterRawInfoHelper.getInstance();
            helper.start(rawLog);

            mCallback.onReadFileSuccess(helper.getFilteredLog());
        } catch (NullPointerException e) {
            e.printStackTrace();
            throw new NullPointerException();
        }
    }


    private void evaluateLine(final RawLog mRawLog, final String line) {
        if (line.contains(Constants.KEY_WORD_INIT_GAME)) {
            final String[] splited = line.trim().split("\\s+");

            //example: 0:00 InitGame: \sv_floodProtect\1\sv_...
            final RawGameLog rawGameLog = new RawGameLog(++COUNTER);
            rawGameLog.setStartEvent(splited[0].trim());

            mRawLog.addGame(rawGameLog);

        } else if (line.contains(Constants.KEY_WORD_SHUTDOWN_GAME)) {
            final RawGameLog rawGameLog = mRawLog.getGamesLogs().get(COUNTER);

            //example: 20:37 ShutdownGame:
            final String[] splited = line.trim().split("\\s+");
            rawGameLog.setEndEvent(splited[0].trim());

        } else if (line.contains(Constants.KEY_WORD_USER_KILL)) {
            final RawGameLog rawGameLog = mRawLog.getGamesLogs().get(COUNTER);

            final String[] splited = line.trim().split("\\s+");

            if (line.contains(Constants.KEY_WORD_WORLD)) {
                //example: 20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT
                final RawPlayerLog rawPlayerLog = new RawPlayerLog(splited[2].trim());

                rawPlayerLog.setVictimId(splited[3].trim());
                rawPlayerLog.setWorld(true);

                rawGameLog.addPlayers(rawPlayerLog);

            } else {
                //example: 22:06 Kill: 2 3 7: Isgalamido killed Mocinha by MOD_ROCKET_SPLASH
                for (RawPlayerLog rawPlayerLog : rawGameLog.getPlayers()) {
                    if (rawPlayerLog.getId().equals(splited[2].trim())) {
                        rawPlayerLog.countKill();
                        rawPlayerLog.setVictimId(splited[3].trim());
                    }
                }
            }

        } else if (line.contains(Constants.KEY_WORD_CLIENT_CONNECT)) {
            final RawGameLog rawGameLog = mRawLog.getGamesLogs().get(COUNTER);

            //example: 20:38 ClientConnect: 2
            final String[] splited = line.trim().split(Constants.KEY_WORD_CLIENT_CONNECT);

            boolean isAdded = false;
            for (RawPlayerLog rawPlayerLog : rawGameLog.getPlayers()) {
                if (rawPlayerLog.getId().equals(splited[1].trim())) {
                    isAdded = true;
                }
            }

            if (!isAdded) {
                final RawPlayerLog rawPlayerLog = new RawPlayerLog(splited[1].trim());
                rawGameLog.addPlayers(rawPlayerLog);
            }

        } else if (line.contains(Constants.KEY_WORD_USER_INFO_CHANGED)) {
            final RawGameLog rawGameLog = mRawLog.getGamesLogs().get(COUNTER);

            //example: 20:38 ClientUserinfoChanged: 2 n\Dono da Bola\t\0\...
            final String[] splitedBySpace = line.trim().split("\\s+");

            for (RawPlayerLog rawPlayerLog : rawGameLog.getPlayers()) {
                if (rawPlayerLog.getId().equals(splitedBySpace[2].trim())) {
                    final String[] splited = line.trim().split(Constants.KEY_WORD_USER_INFO_CHANGED);
                    final String[] changedInfo = splited[1].trim().split("\\\\");

                    rawPlayerLog.setName(changedInfo[1].trim());
                }
            }

        }

    }



}
