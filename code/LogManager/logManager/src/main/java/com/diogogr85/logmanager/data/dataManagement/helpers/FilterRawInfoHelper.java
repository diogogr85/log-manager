package com.diogogr85.logmanager.data.dataManagement.helpers;

import com.diogogr85.logmanager.data.models.FilteredGame;
import com.diogogr85.logmanager.data.models.FilteredLog;
import com.diogogr85.logmanager.data.models.FilteredPlayer;
import com.diogogr85.logmanager.data.models.RawGameLog;
import com.diogogr85.logmanager.data.models.RawLog;
import com.diogogr85.logmanager.data.models.RawPlayerLog;
import com.diogogr85.logmanager.utils.Constants;

import java.util.List;

/**
 * Created by diogogr85 on 06/08/17.
 */

public class FilterRawInfoHelper {

    private static FilterRawInfoHelper sInstance;

    private final FilteredLog mFilteredLog;

    private FilterRawInfoHelper() {
        mFilteredLog = new FilteredLog();
    }

    public static FilterRawInfoHelper getInstance() {
        if (sInstance == null) {
            sInstance = new FilterRawInfoHelper();
        }

        return sInstance;
    }

    public void start(final RawLog rawLog) {
        for (RawGameLog rawGameLog : rawLog.getGamesLogs()) {
            final FilteredGame filteredGame = new FilteredGame(rawGameLog.getId(), rawGameLog.getStartEvent(), rawGameLog.getEndEvent());
            prepareGame(filteredGame, rawGameLog.getPlayers());

            mFilteredLog.addGame(filteredGame);
        }
    }

    private void prepareGame(FilteredGame filteredGame, List<RawPlayerLog> rawPlayers) {
        for (RawPlayerLog rawPlayer : rawPlayers) {
            if (rawPlayer.isWorld()) {

                int worldIndex = -1;
                for (int i = 0; i < filteredGame.getPlayers().size(); i++) {
                    //find out if world object already exists
                    if (rawPlayer.getId().equals(filteredGame.getPlayers().get(i).getId())) {
                        worldIndex = i;
                    }

                    //find world's victim and decrease kill counter
                    if (rawPlayer.getVictimId().equals(filteredGame.getPlayers().get(i).getId())) {
                        filteredGame.getPlayers().get(i).decreaseKill();
                    }
                }

                if (worldIndex >= 0) {
                    filteredGame.getPlayers().get(worldIndex).increaseKill(1);
                } else {
                    //wolrd starts with one kill
                    filteredGame.getPlayers().add(new FilteredPlayer(rawPlayer.getId(), rawPlayer.getName(), 1, rawPlayer.isWorld()));
                }

            } else {
                filteredGame.addPlayers(new FilteredPlayer(rawPlayer.getId(), rawPlayer.getName(), rawPlayer.getKill(), rawPlayer.isWorld()));

            }
        }
    }

    public FilteredLog getFilteredLog() {
        return mFilteredLog;
    }
}
