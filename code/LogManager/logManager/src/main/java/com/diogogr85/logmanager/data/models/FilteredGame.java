package com.diogogr85.logmanager.data.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diogogr85 on 04/08/17.
 */

public class FilteredGame {

    private int mId;
    private String mStartEvent;
    private String mEndEvent;
    private final List<FilteredPlayer> mPlayers;

    public FilteredGame(final int id, final String startEvent, final String endEvent) {
        mId = id;
        mStartEvent = startEvent;
        mEndEvent = endEvent;
        mPlayers = new ArrayList<>();
    }

    public int getId() {
        return mId;
    }

    public String getStartEvent() {
        return mStartEvent;
    }

    public void setStartEvent(String startEvent) {
        mStartEvent = startEvent;
    }

    public String getEndEvent() {
        return mEndEvent;
    }

    public void setEndEvent(String endEvent) {
        mEndEvent = endEvent;
    }

    public List<FilteredPlayer> getPlayers() {
        return mPlayers;
    }

    public void addPlayers(FilteredPlayer player) {
        mPlayers.add(player);
    }
}
