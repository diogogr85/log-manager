package com.diogogr85.logmanager.data.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diogogr85 on 05/08/17.
 */

public class FilteredLog {

    private final List<FilteredGame> mGamesLogs;

    public FilteredLog() {
        mGamesLogs = new ArrayList<>();
    }

    public List<FilteredGame> getGamesLogs() {
        return mGamesLogs;
    }

    public void addGame(final FilteredGame gameLog) {
        mGamesLogs.add(gameLog);
    }

}
