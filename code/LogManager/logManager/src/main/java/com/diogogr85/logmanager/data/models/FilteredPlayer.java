package com.diogogr85.logmanager.data.models;

/**
 * Created by diogogr85 on 05/08/17.
 */

public class FilteredPlayer {

    private String mId;
    private String mName;
    private int mKills;
    private boolean mIsWorld;

    public FilteredPlayer(String id, String name, int kills, boolean isWorld) {
        mId = id;
        mName = name;
        mKills = kills;
        mIsWorld = isWorld;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public boolean isWorld() {
        return mIsWorld;
    }

    public int getKill() {
        return mKills;
    }

    public void increaseKill(int kill) {
        mKills += kill;
    }

    public void decreaseKill() {
        mKills--;

        if (mKills < 0) {
            mKills = 0;
        }
    }

}
