package com.diogogr85.logmanager.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diogogr85 on 06/08/17.
 */
public class GameSummary {

    private Game mGameLog;

    public GameSummary(Game log) {
        mGameLog = log;
    }

    public Game getGameLog() {
        return mGameLog;
    }

    public void setGameLog(Game gameLog) {
        mGameLog = gameLog;
    }


    public static class Game {

        private int mId;
        @SerializedName("total_kills")
        private int mTotalKills;
        @SerializedName("players")
        private List<String> mPlayers;
        @SerializedName("kills")
        private List<KillReport> mKills;

        public Game(final int id) {
            mId = id;

            mPlayers = new ArrayList<>();
            mKills = new ArrayList<>();
        }

        public int getId() {
            return mId;
        }

        public int getTotalKills() {
            return mTotalKills;
        }

        public void setTotalKills(int totalKills) {
            mTotalKills = totalKills;
        }

        public List<String> getPlayers() {
            return mPlayers;
        }

        public void addPlayer(String playerName) {
            mPlayers.add(playerName);
        }

        public List<KillReport> getKills() {
            return mKills;
        }

        public void addKills(KillReport killReport) {
            mKills.add(killReport);
        }
    }
}
