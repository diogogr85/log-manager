package com.diogogr85.logmanager.data.models;

/**
 * Created by diogogr85 on 06/08/17.
 */
public class KillReport {

    private String mName;
    private int mKills;

    public KillReport(String name, int kills) {
        mName = name;
        mKills = kills;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public int getKills() {
        return mKills;
    }

    public void setKills(int mKills) {
        this.mKills = mKills;
    }

}
