package com.diogogr85.logmanager.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diogogr85 on 06/08/17.
 */

public class LogReport {

    @SerializedName("log")
    private final List<GameSummary> mLogs;

    public LogReport() {
        mLogs = new ArrayList<>();
    }

    public List<GameSummary> getLogs() {
        return mLogs;
    }

    public void addSummary(GameSummary summary) {
        mLogs.add(summary);
    }
}
