package com.diogogr85.logmanager.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by diogogr85 on 07/08/17.
 */

public class RankingReport {

//    @SerializedName("game")
    private String mGameName;
//    @SerializedName("ranking")
    private List<KillReport> mKillsRanking;

    public RankingReport(String gameName, List<KillReport> killsRanking) {
        mGameName = gameName;
        mKillsRanking = killsRanking;
    }

    public String getGameName() {
        return mGameName;
    }

    public List<KillReport> getKillsRanking() {
        return mKillsRanking;
    }
}
