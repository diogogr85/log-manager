package com.diogogr85.logmanager.data.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diogogr85 on 04/08/17.
 */

public class RawGameLog {

    private int mId;
    private String mStartEvent;
    private String mEndEvent;
    private final List<RawPlayerLog> mPlayers;

    public RawGameLog(final int id) {
        mId = id;
        mPlayers = new ArrayList<>();
    }

    public int getId() {
        return mId;
    }

    public String getStartEvent() {
        return mStartEvent;
    }

    public void setStartEvent(String startEvent) {
        mStartEvent = startEvent;
    }

    public String getEndEvent() {
        return mEndEvent;
    }

    public void setEndEvent(String endEvent) {
        mEndEvent = endEvent;
    }

    public List<RawPlayerLog> getPlayers() {
        return mPlayers;
    }

    public void addPlayers(RawPlayerLog player) {
        mPlayers.add(player);
    }
}
