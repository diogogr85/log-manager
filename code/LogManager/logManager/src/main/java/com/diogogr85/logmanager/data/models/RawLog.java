package com.diogogr85.logmanager.data.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diogogr85 on 05/08/17.
 */

public class RawLog {

    private final List<RawGameLog> mGamesLogs;

    public RawLog() {
        mGamesLogs = new ArrayList<>();
    }

    public List<RawGameLog> getGamesLogs() {
        return mGamesLogs;
    }

    public void addGame(final RawGameLog gameLog) {
        mGamesLogs.add(gameLog);
    }

    public void editGame(final int index, final RawGameLog gameLog) {
        mGamesLogs.add(index, gameLog);
    }

}
