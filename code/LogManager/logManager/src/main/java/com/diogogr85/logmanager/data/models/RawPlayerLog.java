package com.diogogr85.logmanager.data.models;

/**
 * Created by diogogr85 on 05/08/17.
 */

public class RawPlayerLog {

    private String mId;
    private String mName;
    private int mKills;
    private String mVictimId;
    private String mGunType;
    private String mCauseOfDeath;

    private boolean mIsWorld;

    public RawPlayerLog(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getKill() {
        return mKills;
    }

    public void countKill() {
        mKills++;
    }

    public String getVictimId() {
        return mVictimId;
    }

    public void setVictimId(String mVictimId) {
        this.mVictimId = mVictimId;
    }

    public String getGunType() {
        return mGunType;
    }

    public void setGunType(String gunType) {
        mGunType = gunType;
    }

    public String getCauseOfDeath() {
        return mCauseOfDeath;
    }

    public void setCauseOfDeath(String causeOfDeath) {
        mCauseOfDeath = causeOfDeath;
    }

    public boolean isWorld() {
        return mIsWorld;
    }

    public void setWorld(boolean world) {
        mIsWorld = world;
    }
}
