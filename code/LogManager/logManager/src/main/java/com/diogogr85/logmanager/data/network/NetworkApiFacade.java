package com.diogogr85.logmanager.data.network;

import com.diogogr85.logmanager.data.network.base.NetworkCall;
import com.diogogr85.logmanager.data.network.downloads.DownloadFileNetwork;

import java.io.File;

import okhttp3.ResponseBody;

/**
 * Created by diogogr85 on 08/08/17.
 */

public class NetworkApiFacade {

    public static void downloadFile(final String fileUrl, NetworkCall.Callback<File> callback) {
        final String[] urlArray = fileUrl.split("/");
        final String filePath = urlArray[urlArray.length - 1];

        final String baseUrl = fileUrl.replaceAll(filePath, "");

        final NetworkCall<File> networkCall = new NetworkCall<>(DownloadFileNetwork.build(baseUrl).downloadFile(filePath));
        networkCall.performDownloadEnqueue(callback);
    }

}
