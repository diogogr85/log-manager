package com.diogogr85.logmanager.data.network.base;

import android.content.Context;
import android.util.Log;

import com.diogogr85.logmanager.LogManagerApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by diogogr85 on 08/08/17.
 */

public class NetworkCall<I> {

    public static final String DEFAULT_ERROR = "Ocorreu um erro, por favor verifique a sua conexão e tente novamente.";

    private final Call<ResponseBody> mCall;
    private File mFile;

    public NetworkCall(Call<ResponseBody> responseCall) {
        mCall = responseCall;
    }

    public void performDownloadEnqueue(final Callback<File> callback) {
        mCall.enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    mFile = writeResponseBodyToDisk(response.body());
                    callback.onSuccess(mFile);
                } else {
                    callback.onError(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onError(null, DEFAULT_ERROR);
            }
        });
    }

    public interface Callback<T> {
        void onSuccess(T result);
        void onError(Integer statusCode, String errors);
    }

    private File writeResponseBodyToDisk(ResponseBody body) {
        try {
            final File file = new File(LogManagerApplication.getInstance().getApplicationContext().getDir("logsdir", Context.MODE_PRIVATE) + "/log.txt");

            //We don't need to keep old files
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(NetworkCall.class.getName(), "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return file;
            } catch (IOException e) {
                return null;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return null;
        }
    }


}
