package com.diogogr85.logmanager.data.network.base;

import android.text.TextUtils;

import com.diogogr85.logmanager.LogManagerApplication;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by diogogr85 on 08/08/17.
 */

public class NetworkManager<T> {

    private static final String HEADER_ACCEPT_LANGUAGE = "Accept-Language";
    private static final String HEADER_ACCEPT_LANGUAGE_TYPE = "pt-BR";
    private static final int CONNECTION_TIMEOUT = 45;

    protected T mService;
    private final Class<T> mServiceClass;
    private final String mBaseUrl;

    public NetworkManager(final Class<T> serviceClass, final String restApiEndPoint) {
        mServiceClass = serviceClass;
        mBaseUrl = restApiEndPoint;

        setupNetwork();
    }

    private void setupNetwork() {
        // Define Interceptor for Authentication Headers (Language / Access-Token)
        final Interceptor headerInterceptor = new Interceptor() {

            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request.Builder requestBuilder = chain.request().newBuilder();

                //Accept-Language
                requestBuilder.addHeader(HEADER_ACCEPT_LANGUAGE, HEADER_ACCEPT_LANGUAGE_TYPE);

                return chain.proceed(requestBuilder.build());
            }
        };

        // Define Logging Interceptor for DEBUG mode
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Add the interceptors to OkHttpClient
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.addInterceptor(headerInterceptor);
        httpClientBuilder.addInterceptor(loggingInterceptor);

        OkHttpClient okHttpClient = httpClientBuilder
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addConverterFactory(GsonConverterFactory.create(LogManagerApplication.getGsonInstance()))
                .client(okHttpClient)
                .build();

        mService = retrofit.create(mServiceClass);
    }

}
