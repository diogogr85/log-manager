package com.diogogr85.logmanager.data.network.downloads;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by diogogr85 on 08/08/17.
 */

public interface DownloadFileEndPoint {

    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);

}
