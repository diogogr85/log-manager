package com.diogogr85.logmanager.data.network.downloads;

import com.diogogr85.logmanager.data.network.base.NetworkManager;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by diogogr85 on 08/08/17.
 */

public class DownloadFileNetwork extends NetworkManager<DownloadFileEndPoint> {

    private DownloadFileNetwork(String baseUrl) {
        super(DownloadFileEndPoint.class, baseUrl);
    }

    public static DownloadFileNetwork build(String baseUrl) {
        return new DownloadFileNetwork(baseUrl);
    }

    /**
     * Download method
     */
    public Call<ResponseBody> downloadFile(String fileUrl) {
        return mService.downloadFile(fileUrl);
    }

//    https://futurestud.io/tutorials/retrofit-2-how-to-use-dynamic-urls-for-requests
//    https://futurestud.io/tutorials/retrofit-2-how-to-download-files-from-server

}
