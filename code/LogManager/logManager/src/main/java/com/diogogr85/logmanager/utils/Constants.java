package com.diogogr85.logmanager.utils;

/**
 * Created by diogogr85 on 05/08/17.
 */

public class Constants {

    public static final String KEY_WORD_INIT_GAME = "InitGame:";
    public static final String KEY_WORD_SHUTDOWN_GAME = "ShutdownGame:";
    public static final String KEY_WORD_CLIENT_CONNECT = "ClientConnect:";
    public static final String KEY_WORD_USER_INFO_CHANGED = "ClientUserinfoChanged:";
    public static final String KEY_WORD_USER_KILL = "Kill:";
    public static final String KEY_WORD_USER_ITEM = "Item:";

    public static final String KEY_WORD_WORLD = "<world>";

}
