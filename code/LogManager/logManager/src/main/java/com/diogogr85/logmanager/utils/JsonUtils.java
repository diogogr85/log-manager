package com.diogogr85.logmanager.utils;

import com.diogogr85.logmanager.LogManagerApplication;
import com.diogogr85.logmanager.data.models.GameSummary;
import com.diogogr85.logmanager.data.models.KillReport;
import com.diogogr85.logmanager.data.models.LogReport;
import com.diogogr85.logmanager.data.models.RankingReport;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by diogogr85 on 06/08/17.
 */

public class JsonUtils {

    public static final String toJson(final Object object) {
        return LogManagerApplication.getGsonInstance().toJson(object);
    }

    public static final String toLogReportJson(final LogReport logReport) {
        JSONObject logReportJson = new JSONObject();
        try {
            for (GameSummary gameSummary : logReport.getLogs()) {
                final GameSummary.Game game = gameSummary.getGameLog();

                JSONObject jsonGameObject = new JSONObject();
                jsonGameObject.put("total_kills", game.getTotalKills());
                jsonGameObject.put("players", game.getPlayers());

                JSONObject killsReportJson = new JSONObject();
                for (KillReport killReport : game.getKills()) {
                    killsReportJson.put(killReport.getName(), killReport.getKills());
                }
                jsonGameObject.put("kills", killsReportJson);

                logReportJson.put("game_" + game.getId(), jsonGameObject);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return logReportJson.toString();
    }

    public static final String toRankingReportJson(final List<RankingReport> list) {
        JSONObject rankingReportJson = new JSONObject();

        try {
            for (RankingReport object : list) {
                JSONObject killsObject = new JSONObject();

                for (KillReport killReport : object.getKillsRanking()) {
                    killsObject.put(killReport.getName(), killReport.getKills());
                }

                rankingReportJson.put(object.getGameName(), killsObject);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rankingReportJson.toString();
    }

}
