package com.diogogr85.logmanager.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.diogogr85.logmanager.data.LogManager;
import com.diogogr85.logmanager.R;
import com.diogogr85.logmanager.data.LogReportsCallback;
import com.diogogr85.logmanager.data.analytics.AnalyticsCallback;
import com.diogogr85.logmanager.data.models.LogReport;
import com.diogogr85.logmanager.data.models.RankingReport;
import com.diogogr85.logmanager.data.network.NetworkApiFacade;
import com.diogogr85.logmanager.data.network.base.NetworkCall;
import com.diogogr85.logmanager.utils.JsonUtils;

import java.io.File;
import java.util.List;

import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
