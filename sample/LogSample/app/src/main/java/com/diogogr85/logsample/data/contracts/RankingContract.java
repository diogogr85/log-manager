package com.diogogr85.logsample.data.contracts;

/**
 * Created by diogogr85 on 08/08/17.
 */

public interface RankingContract {

    interface View {
        void onShowProgress(boolean show);
        void onRankingSuccess(final String json);
        void onSummarySuccess(final String json);
    }

    interface Presenter {
        void getRanking(final String fileUrl);
        void getSummary(final String fileUrl);
    }

}
