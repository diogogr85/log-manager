package com.diogogr85.logsample.data.presenters;

import android.content.Context;

import java.lang.ref.WeakReference;

/**
 * Created by diogogr85 on 08/08/17.
 */

public abstract class BasePresenter<V> {

    protected WeakReference<V> view;
    protected Context mContext;

    public BasePresenter(Context context) {
        mContext = context.getApplicationContext();
    }

    public void bindView(final V view) {
        this.view = new WeakReference<V>(view);
    }

    public void unbindView() {
        view = null;
    }

    public V getView() {
        if (view != null) {
            return view.get();
        } else {
            return null;
        }
    }

}
