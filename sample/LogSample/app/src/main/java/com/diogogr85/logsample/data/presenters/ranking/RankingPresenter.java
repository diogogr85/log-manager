package com.diogogr85.logsample.data.presenters.ranking;

import android.content.Context;

import com.diogogr85.logmanager.data.LogManager;
import com.diogogr85.logmanager.data.LogReportsCallback;
import com.diogogr85.logsample.data.contracts.RankingContract;
import com.diogogr85.logsample.data.presenters.BasePresenter;

/**
 * Created by diogogr85 on 08/08/17.
 */

public class RankingPresenter extends BasePresenter<RankingContract.View> implements RankingContract.Presenter {

    public RankingPresenter(Context context) {
        super(context);
    }

    @Override
    public void getRanking(String fileUrl) {
        final RankingContract.View view = getView();
        if (view != null) {
            view.onShowProgress(true);

            LogManager.generateLogRanking(fileUrl, new LogReportsCallback() {
                @Override
                public void onReportsSuccess(String reportsJson) {
                    final RankingContract.View view = getView();
                    if (view != null) {
                        view.onShowProgress(false);
                        view.onRankingSuccess(reportsJson);
                    }
                }

                @Override
                public void onReportsFailed(Integer statusCode, String errorMessage) {
                    final RankingContract.View view = getView();
                    if (view != null) {
                        view.onShowProgress(false);
                    }
                }
            });
        }
    }

    @Override
    public void getSummary(String fileUrl) {
        final RankingContract.View view = getView();
        if (view != null) {
            view.onShowProgress(true);

            LogManager.generateLogSummary(fileUrl, new LogReportsCallback() {
                @Override
                public void onReportsSuccess(String reportsJson) {
                    final RankingContract.View view = getView();
                    if (view != null) {
                        view.onShowProgress(false);
                        view.onSummarySuccess(reportsJson);
                    }
                }

                @Override
                public void onReportsFailed(Integer statusCode, String errorMessage) {
                    final RankingContract.View view = getView();
                    if (view != null) {
                        view.onShowProgress(false);
                    }
                }
            });
        }
    }
}
