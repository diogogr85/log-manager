package com.diogogr85.logsample.views.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.diogogr85.logsample.R;
import com.diogogr85.logsample.data.contracts.RankingContract;
import com.diogogr85.logsample.data.presenters.ranking.RankingPresenter;
import com.diogogr85.logsample.views.ranking.RankingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements RankingContract.View {

    @BindView(R.id.edittext)
    EditText mEditText;
    @BindView(R.id.progress_bar_container)
    View mProgressView;

    private RankingPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mPresenter = new RankingPresenter(getApplicationContext());
        mPresenter.bindView(this);

        mEditText.setText("https://gist.githubusercontent.com/diogogr85/856fb1baa6f8ff5c0fa2d129bf1626d7/raw/2d1b38a3f6136928412bb3a8c61dbf73f9075005/log.txt");

    }

    @OnClick(R.id.show_ranking_button)
    void onClickRanking() {
        final String fileUrl = mEditText.getText().toString();
        if (!TextUtils.isEmpty(fileUrl)) {
            mPresenter.getRanking(fileUrl);
        }
    }

    @OnClick(R.id.show_report_button)
    void onClickSummary() {
        final String fileUrl = mEditText.getText().toString();
        if (!TextUtils.isEmpty(fileUrl)) {
            mPresenter.getSummary(fileUrl);
        }
    }

    @Override
    public void onRankingSuccess(String json) {
        final Intent intent = new Intent(MainActivity.this, RankingActivity.class);
        intent.putExtra("ranking", json);
        startActivity(intent);
    }

    @Override
    public void onSummarySuccess(String json) {
        //TODO
    }

    @Override
    public void onShowProgress(boolean show) {
        if (show) {
            mProgressView.setVisibility(View.VISIBLE);
        } else {
            mProgressView.setVisibility(View.GONE);
        }
    }
}
