package com.diogogr85.logsample.views.ranking;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.diogogr85.logsample.R;
import com.diogogr85.logsample.views.ranking.adapters.RankingAdapter;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diogogr85 on 08/08/17.
 */

public class RankingActivity extends AppCompatActivity {

    @BindView(R.id.ranking_list)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);
        ButterKnife.bind(this);

        final String rankingJson = getIntent().getStringExtra("ranking");

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        try {
            mRecyclerView.setAdapter(new RankingAdapter(rankingJson));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
