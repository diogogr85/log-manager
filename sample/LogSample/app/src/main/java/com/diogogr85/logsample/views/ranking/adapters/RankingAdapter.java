package com.diogogr85.logsample.views.ranking.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diogogr85.logsample.R;
import com.diogogr85.logsample.views.ranking.viewholders.RankingViewHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by diogogr85 on 08/08/17.
 */

public class RankingAdapter extends RecyclerView.Adapter<RankingViewHolder> {

    private final JSONArray mArray;

    public RankingAdapter(String arrayJson) throws JSONException {
        mArray = new JSONArray(arrayJson);
    }

    @Override
    public RankingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_text, parent, false);
        final RankingViewHolder holder = RankingViewHolder.newInstance(itemView);

        return holder;
    }

    @Override
    public void onBindViewHolder(RankingViewHolder holder, int position) {
        try {
            JSONObject jsonObject = mArray.getJSONObject(position);

            holder.setTextView(jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArray.length();
    }
}
