package com.diogogr85.logsample.views.ranking.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.diogogr85.logsample.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by diogogr85 on 08/08/17.
 */

public class RankingViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text)
    TextView mTextView;

    private RankingViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public static RankingViewHolder newInstance(View itemView) {
        return new RankingViewHolder(itemView);
    }

    public TextView getTextView() {
        return mTextView;
    }

    public void setTextView(String text) {
        mTextView.setText(text);
    }
}
